import React from 'react';

class UsernameInput extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: ''
    }
  }

  handleChange = (e) => {
    this.setState({ 
      username: e.target.value
    })
  }

  handleKeyUp = (e) => {
    const ENTER_KEY_CODE = 13
    if (e.keyCode == ENTER_KEY_CODE) {
      this.props.processUsername(e, this.state.username)
    }
  }

  render() {
    const { username } = this.state
    return (
      <div className="input-group">
        <input className="form-control" type="text" onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
        <div className="input-group-append">
          <button type="button" className="btn btn-outline-secondary" onClick={(e) => this.props.processUsername(e, username)}>Go</button>
        </div>
      </div>
    )
  }
}

export default UsernameInput