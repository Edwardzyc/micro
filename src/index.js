import React from 'react';
import ReactDOM from 'react-dom';
import Micro from './Micro';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Micro />, document.getElementById('root'));
registerServiceWorker();
