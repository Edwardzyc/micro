import React from 'react';
import ReactDOM from 'react-dom';
import Micro from './Micro';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Micro />, div);
  ReactDOM.unmountComponentAtNode(div);
});
