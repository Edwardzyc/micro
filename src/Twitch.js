import axios from 'axios'

const client = axios.create({
  headers: {
    'Client-ID': 'n94t5au3iedge0cok055w2aqhhakqj',
    'Accept': 'application/vnd.twitchtv.v5+json'
  }
})

const Twitch = {

  getStreamSummaryForGame: async (game) => {
    const response = await client.get('https://api.twitch.tv/kraken/streams/summary', {
      params: {
        game,
      }
    })
    return response.data
  },

  getUserId: async (username) => {
    const response = await client.get('https://api.twitch.tv/helix/users', {
      params: {
        login: username
      }
    })
    return response.data.data[0]
  },


  getStreamInfo: async (userId) => {
    const response = await client.get(`https://api.twitch.tv/kraken/streams/${userId}`)
    return response.data.stream
  }
}

export default Twitch