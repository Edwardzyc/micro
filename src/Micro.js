import React from 'react';
import './Micro.css';
import Twitch from './Twitch'
import UsernameInput from './UsernameInput'
import PercentViewershipForGame from './components/PercentViewershipForGame'
import TotalStreamingTheGame from './components/TotalStreamingTheGame'
import CurrentViewerCount from './components/CurrentViewerCount'
import UserInformation from './components/UserInformation'

class Micro extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      twitch: null,
      error: null
    }
  }

  updateInformation = async () => {
    const { twitch } = this.state
    const {viewers: totalViewerCountGame, channels: totalPeopleStreamingGame} = await Twitch.getStreamSummaryForGame(twitch.stream.game)
    const stream = await Twitch.getStreamInfo(twitch.user.id)
    this.setState({
      twitch: {
        user: twitch.user,
        stream,
        totalViewerCountGame,
        totalPeopleStreamingGame
      }
    })
  }

  processUsername = async (e, username) => {
    clearInterval(this.interval)
    const user = await Twitch.getUserId(username)
    if (!user) {
      this.setState({
        twitch: null,
        error: 'Could not find the user'
      })
      return 
    }
    const stream = await Twitch.getStreamInfo(user.id)
    if (!stream) {
      this.setState({
        twitch: null,
        error: 'User is not currently streaming'
      })
      return
    }
    this.setState({
      twitch: {
        user,
        stream
      },
      error: null
    })
    this.updateInformation()
    this.interval = setInterval(() => this.updateInformation(), 5000)
  }

  render() {
    const { twitch, error } = this.state
    return (
      <div className="container">
        <h1 className="display-5 mb-2">
          Micro 1.0
        </h1>
        <UsernameInput processUsername={this.processUsername}/>
        { error && 
          <small>{error}</small>
        }
        { twitch &&
          <React.Fragment>
            <UserInformation twitch={twitch}/>
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm">
                  <TotalStreamingTheGame twitch={twitch}/>
                  </div>
                  <div class="col-sm">
                    <CurrentViewerCount twitch={twitch}/>
                  </div>
                  <div class="col-sm">
                    <PercentViewershipForGame twitch={twitch}/>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        }
      </div>
    );
  }
}

export default Micro;
