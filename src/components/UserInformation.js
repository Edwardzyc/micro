import React from 'react';

const UserInformation = (props) => {
  return (
    <blockquote className="blockquote mt-3">
      <p className="mb-0">{props.twitch.user.display_name}</p>
      <small className="text-muted">Playing [{props.twitch.stream.game}]</small>
    </blockquote>
  )
}

export default UserInformation