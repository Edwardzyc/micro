import React from 'react';

const TotalStreamingTheGame = (props) => {
  return (
    <React.Fragment>
      <div>{props.twitch.totalPeopleStreamingGame}</div>
      <small>People streaming</small>
    </React.Fragment>
  )
}

export default TotalStreamingTheGame