import React from 'react';

const PercentViewershipForGame = (props) => {
  const percent = (props.twitch.stream.viewers / props.twitch.totalViewerCountGame * 100).toFixed(2)
  if (isNaN(percent)) {
    return null
  }
  return (
    <React.Fragment>
      <div>{percent}%</div>
      <small>Viewership percent</small>
    </React.Fragment>
  )
}

export default PercentViewershipForGame