import React from 'react';

const CurrentViewerCount = (props) => {
  return (
    <React.Fragment>
      <div>{props.twitch.stream.viewers}</div>
      <small>Current viewers</small>
    </React.Fragment>
  )
}

export default CurrentViewerCount
